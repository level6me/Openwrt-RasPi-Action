#!/bin/bash
#=================================================
# Description: DIY script
# Lisence: MIT
#=================================================
# Modify default IP
#sed -i 's/192.168.1.1/192.168.50.5/g' package/base-files/files/bin/config_generate

# Add default settings.
echo -e "\033[32mAdd default settings. \033[0m"
cd package/community
git clone https://github.com/SuLingGG/default-settings

# Fall back to project root
echo -e "\033[32mFall back to project root. \033[0m"
cd ../.. && pwd

# Change timezone
echo -e "\033[32mChange timezone \033[0m"
sed -i "s/'UTC'/'CST-8'\n        set system.@system[-1].zonename='Asia\/Shanghai'/g" package/base-files/files/bin/config_generate

# Change default theme
echo -e "\033[32mChange default theme \033[0m"
sed -i 's/config internal themes/config internal themes\n    option Argon  \"\/luci-static\/argon\"/g' feeds/luci/modules/luci-base/root/etc/config/luci

# Remove bootstrap theme
echo -e "\033[32mRemove bootstrap theme \033[0m"
sed -i '/set luci.main.mediaurlbase=\/luci-static\/bootstrap/d' feeds/luci/themes/luci-theme-bootstrap/root/etc/uci-defaults/30_luci-theme-bootstrap

# Open WiFi
echo -e "\033[32mOpen WiFi \033[0m"
sed -i 's/disabled=1/disabled=0/g' package/kernel/mac80211/files/lib/wifi/mac80211.sh

exit 0
