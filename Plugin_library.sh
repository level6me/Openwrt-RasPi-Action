#!/bin/bash
#=================================================
# Description: Plugin Library Script
#=================================================

# Add lienol package sources.
#echo -e "\033[32mAdd lienol package sources. \033[0m"
#echo "src-git lienol https://github.com/Lienol/openwrt-package" >> feeds.conf.default

# Clone Lean's latest sources. (use --depth=1) 
echo -e "\033[32mClone Lean's latest sources. (use --depth=1). \033[0m"
cd package
git clone --depth=1 https://github.com/coolsnowwolf/lede

# Copy Lean's packages to ./package/lean.
echo -e "\033[32mCopy Lean's packages to ./package/lean. \033[0m"
mkdir lean
cd lede/package/lean
cp -r {adbyby,antfs-mount,automount,baidupcs-web,ddns-scripts_aliyun,ddns-scripts_dnspod,ipt2socks,luci-app-adbyby-plus,luci-app-baidupcs-web,luci-app-dnspod,luci-app-familycloud,luci-app-kodexplorer,luci-app-mwan3helper,luci-app-n2n_v2,luci-app-netdata,luci-app-nps,luci-app-syncdial,luci-app-unblockmusic,luci-app-verysync,luci-app-vsftpd,luci-app-xlnetacc,luci-app-zerotier,n2n_v2,npc,pdnsd-alt,shadowsocksr-libev,trojan,UnblockNeteaseMusic,v2ray,verysync,vsftpd-alt} "../../../lean"
cp -r {luci-app-smartdns,smartdns} "../../../"
cp -r ../kernel/antfs ../../../kernel
cd "../../../"
rm -rf lede

# Clone community packages to package/community
echo -e "\033[32mClone community packages to package/community \033[0m"
mkdir community && cd community

# Add luci-app-dockerman.
echo -e "\033[32mAdd luci-app-dockerman. \033[0m"
git clone https://github.com/lisaac/luci-lib-docker
git clone https://github.com/lisaac/luci-app-dockerman

# Add luci-app-frpc.
echo -e "\033[32mAdd luci-app-frpc. \033[0m"
git clone https://github.com/SuLingGG/luci-app-frpc

# Add luci-app-diskman.
echo -e "\033[32mAdd luci-app-diskman. \033[0m"
git clone https://github.com/lisaac/luci-app-diskman
mkdir parted
cp luci-app-diskman/Parted.Makefile parted/Makefile

# Add luci-app-vssr
echo -e "\033[32mAdd luci-app-vssr \033[0m"
git clone https://github.com/jerrykuku/luci-app-vssr

# Add luci-theme-argon
echo -e "\033[32mAdd luci-theme-argon \033[0m"
git clone https://github.com/jerrykuku/luci-theme-argon

# Fall back to project root
echo -e "\033[32mFall back to project root. \033[0m"
cd ../.. && pwd

exit 0
